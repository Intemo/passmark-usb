//Passmark USB2 Linux example program
//PassMarkUSB.h
//Copyright PassMark Software 2010-2014
//www.passmark.com

#ifndef PASSMARK_USB_H
#define PASSMARK_USB_H

//Include Linux USB library
#include <libusb-1.0/libusb.h>


#define BUF_256 256
#define BUF_128 128

//Vendor and product IDs for PassMark USB plugs
#define LOOPBACK_VENDOR_ID 0x0403
#define LOOPBACK_PRODUCT_ID 0xff0a

//
// USB2 Firmware definitions
//
// Firmware vendor command to change modes
#define CHANGEMODE		0xB0	// Passmark vendor command setup in firmware
#define	CHANGEMODEIN	1		// Number of parameters sent to this firmware command
#define CHANGEMODEOUT	2

#define CHANGEMODEOUTSTAT 50
#define INITIALISATION 0
#define LOOPBACK 1
#define BENCHMARK 2
#define STATISTICS 3
#define CHANGELEDS 4

#define EPLOOPOUT 0x02 //Loopback out endpoint
#define EPLOOPIN 0x86 //Loopback in endpoint

#define EPBENCHIN 0x88  //Benchmark in endpoint
#define EPBENCHOUT 0x04 //Benchmark out endpoint
#define EPHISTORY 0x81  //history report endpoint

#define MAXNUMUSBPORTS 20
#define MAXSERIALNUMLEN 20

#define MAX_DRIVER_NAME 64

//
// Host definitions
//
#define FSFIFOSIZE 64				// Size of blocks handled by USB device (chuncked into these blocks at a lower layer)
#define HSFIFOSIZE 512				// Size of blocks handled by USB device (chuncked into these blocks at a lower layer)
#define LOOPBACKFSBUFFERSIZE 64		// Buffersize for loopback in FullSpeed mode = FIFO buffer size
#define LOOPBACKHSBUFFERSIZE 512	// Buffersize for loopback in HighSpeed mode = FIFO buffer size - 4095 is linux limit?

#define BENCHMARKFSBUFFERSIZE 2048	// Buffersize for benchmark in FullSpeed mode = worst case history buffer full
#define BENCHMARKHSBUFFERSIZE 32768	// Buffersize for benchmark in HighSpeed mode = worst case history buffer full
#define BENCHMARKHISTBUFFERSIZE 64  // Buffersize for benchmark History report

#define READWRITECYCLES 10		//Number of times bencmark test will send or read data during a cycle
#define BENCHMARKCYCLES 10		//Number of times benchmark will run read/write cycle


//Data Structures
typedef struct USBInfo
{
	char usbSerial[MAXSERIALNUMLEN];
	int bus;
	int port;
	int speed;
} *USBInfoPtr;

typedef enum _DATA_PATTERN {
	INCREMENTINGBYTE,
		RANDOMBYTE,
		CONSTANTBYTE
} DATA_PATTERN;

typedef struct _BENCHMARK_RESULTS
{
	float MaxReadSpeed;
	float MaxWriteSpeed;
} BENCHMARK_RESULTS;

//Globals
libusb_context* USB_context; 
USBInfo	usbInfo[MAXNUMUSBPORTS+1];
	
//Function defs
int GetUSBPortsInfo();

int SendVendorCommand ( libusb_device_handle* udev, int ReqMode, int Parameter, unsigned char* bufferout, int bufferSize);

bool ConnectUSB2Plug(int usbPlugIndex, libusb_device_handle **handle_udev,  
                    unsigned long *MaxTransferSize, unsigned long *CurrentTransferSize, 
                     unsigned char **inBuffer, unsigned char **outBuffer);

int GetUSBDeviceInfo ( libusb_device_handle *udev, char* serial, unsigned int serial_size, char* desc, unsigned int desc_size );

void wait_USB (int wait_time) ;

void BenchmarkTest(libusb_device_handle *handle_udev, int usbIndex);
void LoopbackTest(libusb_device_handle *handle_udev, int usbIndex, int maxTransferSize, unsigned char *inBuffer, unsigned char *outBuffer);

#endif //PASSMARK_USB_H