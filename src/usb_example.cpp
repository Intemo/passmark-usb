//Passmark USB2 Linux example program
//usb_example.cpp
//Copyright PassMark Software 2010-2014
//www.passmark.com

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include "PassMarkUSB.h"

//Example application to run a loopback and benchmark test
//Can use command line argument to pass the index (0 based) in usbinfo of plug to test or it will default to 0
int main(int argc, char *argv[])
{
	libusb_device_handle *handle_udev; //handle to USB2 plug after being connected 
	unsigned long MaxTransferSize = 0;
	unsigned long CurrentTransferSize = 0;
	unsigned char *inBuffer = NULL;
	unsigned char *outBuffer = NULL;
	int plugindex = 0; //Plug under testing (index in usbInfo)
	
	//Process command line to get plug index
	if(argc > 1)
	{
		plugindex = atoi(argv[1]); 
	}
	
	int numUSB2PLugs = 0;
	//Make sure USB2 list is empty
	memset(usbInfo, 0, sizeof(usbInfo));
	
	//Initialise USB library
	int ret = libusb_init(&USB_context);
	
	if(ret < 0)
	{
		printf("Failed to initialise libusb, error: %d\n", ret);
		return 0 ;
	}
	
	//Get a list of all USB2 loopback devices on system
	numUSB2PLugs = GetUSBPortsInfo();
	
	if(numUSB2PLugs < 1)
	{
		printf("No USB2 loopback plugs found\n");
		return 0 ;
	}
	
	for(int count =0; count < numUSB2PLugs; count++)
	{
		printf("Found plug %s at %d:%d\n", usbInfo[count].usbSerial, usbInfo[count].bus, usbInfo[count].port);
	}
	
	//Check plug index is valid
	if(plugindex >= numUSB2PLugs)
		plugindex = 0;
	
	//Connect to selected plug
	if(ConnectUSB2Plug(plugindex, &handle_udev,  &MaxTransferSize, &CurrentTransferSize,  &inBuffer, &outBuffer) == false)
	{
		printf("Couldn't connect to plug\n");
		return 0;
	}
		
	//Short sleep before tests to display available plugs
	printf("Testing plug %s starts in 5 seconds\n", usbInfo[plugindex].usbSerial);
	sleep(5);

	//Run a loopback test
	printf("Loopback testing plug %s\n", usbInfo[plugindex].usbSerial);
	LoopbackTest(handle_udev, plugindex, MaxTransferSize, inBuffer, outBuffer);

	//Run a benchmark test 
    printf("Benchmarking plug %s (5 write cycles, 5 read cycles) \n", usbInfo[plugindex].usbSerial);
	BenchmarkTest(handle_udev, plugindex);
	
	//Relase device and close handle
	libusb_release_interface(handle_udev, 0);
	libusb_close(handle_udev);
	
	//Clear up buffers
	if (outBuffer)
		delete [] outBuffer;
	if (inBuffer)
		delete [] inBuffer;
	
	return 0;
}



//SendVendorCommand
//
// Send a Passmark specific vendor command
//	in: 
//		USB2 device handle (where to send the command)
//		The Requested mode
//		Parameter to be sent to USB device
//			- currently only used to indicate which LEDs to turn on/off
//  out:Pointer to the buffer of data returned from the device
//		For LOOPBACK || BENCHMARK || CHANGELEDS commands this is:
//			Size of buffer == 2
//			buffer[0] is response to change vendor command. Returns requested command if OK. Returns FF if NOK.
//			buffer[1] is T if device is high speed, F= if Fullspeed
//
//		For STATISTICS command this is:
//			Size of buffer == 0x32
//			buffer[0] is the number of bus errors and the interrupt limit for errors if OK. Returns FF if NOK.
//			buffer[1] is T if device is high speed, F= if Fullspeed
//			buffer[2] is the firmware version number (0 = version 1, 2 = version 2, 3 = version 3...)
//			buffer[3..n] is a PASSMARK copyright string
//
//	returned: Number of bytes returned from the device, 0 if error, -1 is ERROR_NOT_ENOUGH_MEMORY error

int SendVendorCommand ( libusb_device_handle* udev,int ReqMode, int Parameter, unsigned char* bufferout, int bufferSize )
{
	int iNumOutParameters = CHANGEMODEOUT;
	int bytesReturned = 0;
	int requestValue = 0;
	
	//Check a valid output buffer is passed
	if(bufferout == NULL)
		return 0;
	
	if (ReqMode == STATISTICS) 
	{
		//Check buffer is big enough
		if(bufferSize < CHANGEMODEOUTSTAT)
			return 0;
		
		requestValue = STATISTICS;
		iNumOutParameters = CHANGEMODEOUTSTAT;
	}
	else if (ReqMode == BENCHMARK) 
	{
		requestValue = BENCHMARK;
	}
	else if (ReqMode == CHANGELEDS) 
	{
		requestValue = Parameter*256+CHANGELEDS;
	}
	else 
	{
		requestValue = LOOPBACK;
	}
	
	//Check buffer is big enough for the other modes (STATISTICS check previous)
	if(bufferSize < CHANGEMODEOUT)
		return 0;
	
	//Clear buffer passed for output	
	memset(bufferout, 0, bufferSize);
	
	//send control message
	bytesReturned = libusb_control_transfer(udev, LIBUSB_REQUEST_TYPE_VENDOR, CHANGEMODE, requestValue, 0, bufferout, iNumOutParameters, 1000);		
	
	return(bytesReturned);
	
}

//Name:	GetUSB2DeviceInfo
//	Gets the serial number and the device description a specified  USB2 plug
//  The string descriptor is obtained using two separate calls.  The
//  first call is done to determine the size of the entire string descriptor,
//  and the second call is done with that total size specified.
//  For more information, please refer to the USB Specification, Chapter 9.
//Inputs
//      Udev - USB device
//	MaxSerialLen = Max length of buffer for serial num
//	MaxDescLen = Max length of buffer for description
//Outputs
//	Device serial number string
//	Device description string
//Returns
//	1 if OK
//	2 if OK, but EEPROM on device doesn't look to be programmed correctly
//	0 if error
int GetUSBDeviceInfo ( libusb_device *udev, char* serial, unsigned int serial_size, char* desc, unsigned int desc_size )
{
	
	char buf1[128];
	char buf2[128];
	char buf3[128];
	int ret = 0;
	
	//Make sure buffers are large enough and pointers passed are usable
	if ( serial == NULL || desc == NULL || serial_size <= 16 || desc_size <= 64 || udev == NULL )
		return 0;
	
	sprintf ( serial, "N/A" );
	sprintf ( desc, "N/A" );
	
	libusb_device_descriptor dev_descriptor;
	ret = libusb_get_device_descriptor ( udev, &dev_descriptor );
	if ( ret != 0 )
	{
		return 0;
	}
	
	libusb_device_handle* udev_handle;
	ret = libusb_open ( udev, &udev_handle );
	if ( ret != 0 )
	{
		return 0;
	}
	
	if ( libusb_get_string_descriptor_ascii ( udev_handle, dev_descriptor.iManufacturer, ( unsigned char* ) buf1, 64 ) < 0 )
	{
		libusb_close ( udev_handle );
		return 0;
	}
	
	if ( libusb_get_string_descriptor_ascii ( udev_handle, dev_descriptor.iProduct, ( unsigned char* ) buf2, 64 ) < 0 )
	{
		libusb_close ( udev_handle );
		return 0;
	}
	
	if ( libusb_get_string_descriptor_ascii ( udev_handle, dev_descriptor.iSerialNumber, ( unsigned char* ) buf3, 64 ) < 0 )
	{
		libusb_close ( udev_handle );
		return 0;
	}
	
	libusb_close ( udev_handle );
	
	if ( strlen ( buf1 ) + strlen ( buf2 ) + 2 > desc_size || strlen ( buf3 ) +1 > serial_size )
		return 0;
	
	
	sprintf ( desc, "%s %s", buf1, buf2 );
	
	strcpy ( serial, buf3 );
	
	if ( ( strncasecmp ( serial, "PM", 2 ) == 0 && strncasecmp ( desc, "PASS", 4 ) == 0 ) ||
	     ( strncasecmp ( serial, "TH", 2 ) == 0 ) ) 
		return 1;
	else
		return 2;
	
	return 0;
}

//Iterate through available USB plugs
//match to /proc/bus/usb/devices so we can get the Bus/Port number plug is currently on
//Returns number of USB2 loopbacks found
int GetUSBPortsInfo()
{
	char *buf = NULL;
	char *tmpPtr = NULL;
	char *tmpUSBptr = NULL; 
	char *tmpPosPtr = NULL;
	char *tmpPosPtr2 = NULL;
	char *tmpEndLinePtr = NULL;
	char tmpSerial[MAXSERIALNUMLEN];
	char tmpLoc[5];
	int count = 0 ;
	
	FILE* fp = NULL;
	int size = 0;
	int numUSBOccurrences = 0;	//Total number of usb plug occurrences in devices file
	int len = 0;
	bool found = false;
	
	memset(usbInfo, 0, sizeof(usbInfo));
	
	fp = fopen("/proc/bus/usb/devices", "r");
	if(fp == NULL)
	{
		//Might not exist so try other location (eg Fedora 16 and newer)
		fp = fopen("/sys/kernel/debug/usb/devices", "r");
		if(fp == NULL)
		{
			free(buf);
			return 0;
		}
	}
	
	//Count bytes in file, can't use fstat as it doesn't always work as expected
	while(!feof(fp))
	{
		if(fgetc(fp) == EOF)
			break;
		size++;
	}
	rewind(fp);
	
	buf = (char*)calloc(size+1, 1);
	
	if(buf == NULL)
	{
		fclose(fp);
		return 0;
	}
	
	//Search through the buffer for occurances of each USB plug
	int read = fread(buf, sizeof(char), size, fp);
	if(read > 0)
	{
		//Count how many occurances there are
		tmpPtr = strstr(buf, "Manufacturer=PASSMARK");
		while(tmpPtr != NULL)
		{				
			tmpPtr = strstr(tmpPtr+1, "Manufacturer=PASSMARK");
			numUSBOccurrences++;
		}
		
		if(numUSBOccurrences > 0)
		{	
			tmpPtr = buf;
			
			for(count = 0; count < numUSBOccurrences && count < MAXNUMUSBPORTS; count++)
			{
				memset(tmpSerial, 0, MAXSERIALNUMLEN);
				
				if(tmpPtr == NULL)
					break;
				
				//Find first Manufacturer=PASSMARK instance
				tmpPtr = strstr(tmpPtr, "Manufacturer=PASSMARK");
				
				if(tmpPtr == NULL)
					break;
				
				//Find next serial number
				tmpUSBptr = strstr(tmpPtr, "SerialNumber=");
				
				if(tmpUSBptr == NULL)
					break;
				
				tmpEndLinePtr = strchr(tmpUSBptr, '\n');
				len = tmpEndLinePtr - tmpUSBptr - 13; //-13 to counter "SerialNumber: "
				
				//Check length is ok, otherwise could be another USB device and not passmark plug
				if(len > MAXSERIALNUMLEN)
				{
					buf = tmpPtr+1;
					continue;
				}
				
				//Save tmp serial
				memset(tmpLoc, 0, 5);
				strncpy(tmpSerial, tmpUSBptr+13, len);
				
				
				//Save serial
				strcpy(usbInfo[count].usbSerial, tmpSerial);
				
				//Need to search for Bus= until it is close to current pos
				tmpPosPtr = strstr(buf, "Bus=");
				
				while(tmpPosPtr != NULL && tmpPosPtr < tmpPtr)
				{
					tmpPosPtr2 = tmpPosPtr;
					tmpPosPtr = strstr(tmpPosPtr+1, "Bus=");
				}
				
				//tmpPosPtr = strrstr(tmpPtr, "Bus=");
				strncpy(tmpLoc, tmpPosPtr2+4, 2);
				usbInfo[count].bus = atoi(tmpLoc);
				
				tmpPosPtr = strstr(tmpPosPtr2, "Port=");
				strncpy(tmpLoc, tmpPosPtr+5, 2);
				usbInfo[count].port = atoi(tmpLoc) + 1;
				
				//Get speed
				tmpPosPtr = strstr(tmpPosPtr2, "Spd=");
				strncpy(tmpLoc, tmpPosPtr+4, 4);
				usbInfo[count].speed =  atoi(tmpLoc);
				
				tmpPtr = tmpPtr+1;
				found = false;
			}
		}
		
	}
	
	fclose(fp);
	free(buf);
	
	return count;
}

//Name:	ConnectUSB2Plug
// Open a handle to the matching USB plug in usbInfo[usbPlugIndex], sets it to loopback mode and
// gets plug ready for testing.
//Inputs
// usbPlugIndex - index of plug to test in usbInfo global
//Outputs
// libusb_device_handle **handle_udev
// unsigned long *MaxTransferSize
// unsigned long *CurrentTransferSize 
// unsigned char **inBuffer
// unsigned char **outBuffer
// USBInfo* usbInfo
//Returns
//	true on sucess
//	false on failure

bool ConnectUSB2Plug(int usbPlugIndex, libusb_device_handle **handle_udev, 
                     unsigned long *MaxTransferSize, unsigned long *CurrentTransferSize, 
                     unsigned char **inBuffer, unsigned char **outBuffer)
{
	int	Ret = 0;
	unsigned char bufferout[CHANGEMODEOUTSTAT] = {0};
	char USBSerial[50] = {'\0'};
	char USBDesc[100] = {'\0'};
	bool found = false;
	libusb_device **list;
	libusb_device_descriptor descriptor;
	
	if(USB_context == NULL)
	{
		printf("USB_context is null - libusb failed to load or hasn't been loaded\n"); 
		return false;	
	}
	
	int numdevices = libusb_get_device_list ( USB_context, &list );
	
	if ( numdevices < 1 )
	{
		if(numdevices == 0)
			printf("No USB devices returned\n"); 
		else //An error occured
			printf("FailediHighSpeed to get list of USb devices: %d\n", numdevices); 
		
		return false;
	}
	if (*outBuffer)			
		delete *outBuffer;
	
	//Find requested USB plug
	libusb_device *device;
	for ( int count = 0; count < numdevices && found != true; count++ )
	{
		device = list[count];
		if ( libusb_get_device_descriptor ( device, &descriptor ) == 0 )
		{
			if ( descriptor.idVendor == LOOPBACK_VENDOR_ID )
			{
				if ( descriptor.idProduct == LOOPBACK_PRODUCT_ID )
				{
					if ( GetUSBDeviceInfo ( device, USBSerial, 50, USBDesc, 100 ) > 0 )
					{
						if ( strcmp ( USBSerial, usbInfo[usbPlugIndex].usbSerial ) == 0 )
						{
							found = true;
							break;							
						}
						
					}
				}
				
			}
		}
	}
	
	if(found == false)
	{
		printf("Did not find Passmark USB plug %s\n", usbInfo[usbPlugIndex].usbSerial);
		libusb_free_device_list ( list, 1 );
		return false;
	}
	
	int ret = libusb_open ( device, handle_udev );
	if ( ret != 0 )
	{
		//Couldn't connect
		printf("Could not open device, error: %d\n", ret);
		libusb_free_device_list ( list, 1 );
		return false;
	}
	
	libusb_free_device_list ( list, 1 );
	
	//Claim interface
	ret = libusb_claim_interface ( *handle_udev, 0 );
	if ( ret  != 0 )
	{
		//failed - try detaching and claiming again
		ret = libusb_detach_kernel_driver ( *handle_udev, 0 );
		ret = libusb_claim_interface ( *handle_udev, 0 );
		if ( ret != 0 )
		{
			//Could not claim device interface
			printf("Could not claim device interface, error: %d\n", ret);
			return false;
		}
	}
		//Clear halt state
	libusb_clear_halt ( *handle_udev,  EPLOOPOUT );
	libusb_clear_halt ( *handle_udev, EPLOOPIN );	
	
	Ret = SendVendorCommand ( *handle_udev, LOOPBACK,0, bufferout, CHANGEMODEOUTSTAT); //Set Firmware to Loopback Mode
	if ( Ret == 0xFF || Ret <=0  )
	{	
		return false;
	}
	
	// initialise the transfer size and allocate the transfer buffers/ Set the speed for the test window dsiplay
	*MaxTransferSize = LOOPBACKHSBUFFERSIZE;
	*CurrentTransferSize = *MaxTransferSize;
	
	if (*outBuffer)
		delete [] *outBuffer;
	if (*inBuffer)
		delete [] *inBuffer;
	
	*outBuffer = new unsigned char[*MaxTransferSize];
	*inBuffer = new unsigned char[*MaxTransferSize];
	
	if (*outBuffer == NULL || *inBuffer == NULL) 
	{
		printf("Failed to allocate buffers\n");
		return false;
	}
	
	return true;
}

//Break a sleep into smaller chunks, enables a check to be performed and break the sleep early if required
void wait_USB (int wait_time) 
{
	while (wait_time > 0) 
	{
		usleep(100000); 
		wait_time -= 100;
		//Can add a check here for multi threading apps to break the wait if the overall tests are stopped
	}	
}


//Run a Loopback test, stops after sending 1000 packets
void LoopbackTest(libusb_device_handle *handle_udev, int usbIndex, int maxTransferSize, unsigned char *inBuffer, unsigned char *outBuffer)
{	
	bool error_USB = 0;
	bool packetIDMatch = false;
	int iBytesIncorrect = 0;
	int writeRes = 0;
	int readRes = 0;
	int numErrors = 0;
	int numIgnored = 0;
	unsigned long	currentTransferSize = maxTransferSize;
	unsigned long	i = 0;
	unsigned long	pattern = 1; 
	unsigned long	testPass = 0; 
	unsigned long	numPktsSent =0;
	unsigned long	numPktsRec = 0;
	unsigned long 	totalBytesSent = 0;
	unsigned long 	totalBytesRec = 0;
	int BytesWritten = 0 ;
	int BytesRead = 0 ;
	unsigned char buffercmd[CHANGEMODEOUTSTAT] = {0};

	bool testRunning = true;
	DATA_PATTERN gDataPattern;
	
	//Clear bus error count (by reading the stats)
	SendVendorCommand(handle_udev, STATISTICS,0, buffercmd, CHANGEMODEOUTSTAT);
	
	//Turn off error LED (and Tx/Rx) 00 00 00 11 off off off on - Turn off all LEDS except Highspeed
	SendVendorCommand(handle_udev, CHANGELEDS, 0x03, buffercmd, CHANGEMODEOUTSTAT); 
	
	//Set data pattern for test
	gDataPattern = RANDOMBYTE;
	
	//Now enter the test loop - currently send/recv 100 packets
	while(testRunning && testPass < 1000)
	{	
		//Increment packet number
		testPass++;
		
		// initialize the buffer to send based on the pattern picked
		switch(gDataPattern)
		{
		case INCREMENTINGBYTE:
			for (i=0;i<currentTransferSize;i++) 
				outBuffer[i] = (unsigned char) pattern++;
			break;
		case CONSTANTBYTE:
			for (i=0;i<currentTransferSize;i++)
				outBuffer[i] = (unsigned char) pattern;
			break;
		case RANDOMBYTE:
			for (i=0;i<currentTransferSize;i++)
				outBuffer[i] = rand();
			break;				
			
		} 
		
		//insert a packet number in the first WORD 
		unsigned long * pBCurrent = (unsigned long *) &outBuffer[0];	
		*pBCurrent = testPass;
		
		// initialize the in buffer
		memset(inBuffer, 0, maxTransferSize);	
		
		//
		//Write packet and check it was sent
		//
		writeRes = 0;
		BytesWritten = 0;
		
		writeRes = libusb_bulk_transfer ( handle_udev, EPLOOPOUT, outBuffer, currentTransferSize, &BytesWritten, 1000 );
		
		if (writeRes != 0)
		{
			error_USB = true;
			printf("Data packet %lu send failed - libusb_bulk_transfer failed %d : BytesWritten: %d\n",numPktsSent+1,  readRes, BytesWritten);	
		}
		else
		{
			//bytesWritten = writeRes;
			totalBytesSent += BytesWritten;
			numPktsSent++;	
		}
		
		if (BytesWritten != (int) currentTransferSize) 
		{
			error_USB = true;
			printf("Data packet %lu send failed - %d bytes sent - expected %lu\n", numPktsSent, BytesWritten, currentTransferSize);			
			continue;
		}
		
		//Read packet and check it was the expected packet with the expected number of bytes
		packetIDMatch = false;
		numIgnored = 0;
		while(packetIDMatch == false && error_USB == false && numIgnored < 5)
		{
			
			readRes = 0;
			BytesRead = 0;
			
			readRes = libusb_bulk_transfer(handle_udev, EPLOOPIN, inBuffer, currentTransferSize, &BytesRead, 1000);

			if (readRes != 0)
			{
				printf("Data packet %lu receive failed - libusb_bulk_transfer failed %d\n", numPktsSent, readRes);
				error_USB = true;
				wait_USB(100);
			}
			else
			{
				//Update the number of bytes received
				totalBytesRec += BytesRead;
			}
			
			
			if (BytesRead != (int) currentTransferSize) 
			{
				error_USB = true;
				printf("Data packet %lu receive failed - %d bytes read - expected %lu\n", numPktsSent, BytesRead, currentTransferSize);			
				continue;
			} 
			
			//Verify packet
			iBytesIncorrect = 0;
			for (i=0;i<currentTransferSize;i++) 
			{
				if (inBuffer[i] != outBuffer[i])
					iBytesIncorrect++;
			}
			
			if (iBytesIncorrect > 0)
			{
				
				//Check packet number matches
				unsigned long * pulOut = (unsigned long *) &outBuffer[0];
				unsigned long * pulIn = (unsigned long *) &inBuffer[0];	
				
				//If the data read is wrong, then maybe the last read failed and we are now reading data for the previous write.
				if(pulOut != pulIn)
				{
					if(numIgnored < 5)
					{
						printf ("Ignoring packet - ID mismatch\n");
					}
					else
					{
						printf ("Error - 5 packets in a row with an ID mismatch\n");
						error_USB = true;
					}
					numIgnored++;
				}
				else
				{
					error_USB = true;
					packetIDMatch = true;
					printf ("Incorrect byte in received packet %lu - byte %lu - expected %d - received %d\n\n", numPktsSent, i, outBuffer[i], inBuffer[i]);
				}
				
				continue;
			}
			else
			{
				packetIDMatch = true;
				//Increment received count
				numPktsRec++;
			}
		}
		
		if(error_USB)
		{
			numErrors++;
		}
		
		printf ("Error Count: %d - Packets sent: %lu - Bytes sent: %lu - Bytes received %lu\n", numErrors, numPktsSent, totalBytesSent, totalBytesRec);
	}

	return;
}


//Run a benchmark test, stops after BENCHMARKCYCLES cycles
void BenchmarkTest(libusb_device_handle *handle_udev, int usbIndex)
{
	bool 	ReadPhase = true;
	int 	iRet = 0;
	int	error_USB = false;
	int 	TestPass = 1;
	unsigned char	bufferout[CHANGEMODEOUTSTAT] = {0};
	unsigned char*	HistBuffer = NULL;
	unsigned char*  outBuffer = NULL;
	unsigned char*  inBuffer = NULL;
	int		FIFOSize = 0;
	int	dwBytesWritten = 0, dwBytesRead = 0;
	int 	writeRes = 0, readRes;
	int	CurrentHistTransferSize = FSFIFOSIZE;
	int	iTotalHistory = 0, MaxHistory = 0, MinHistory = 0, NumFrames = 0, History[64] = {0};
	int	iTotalHistoryMeasured = 0, iFirstFrameVal = 0, iLastFrameVal = 0;
	float	flAveHistoryMeasured = 0.0, flAveHistoryPossible = 0.0;
	float	MaxRate = 0.0, MinRate = 0.0, AverageRate =0.0;
	float	OverallMaxRate =0.0, TotalOfAverageRate = 0.0, OverallAverageRate =0.0, OverallMinRate = 99999.0;
	int	iLastFrameIndex = 0;
	bool	TestInvalid = false;
	int	iNumTestInvalid = 0;
	float 	g_flMaxReadRate = 0, g_flMaxWriteRate = 0, g_flMaxRate = 0;
	int benchmarkCycle = 0;
	bool g_bHighSpeed =  false;
	int MaxTransferSize = BENCHMARKHSBUFFERSIZE;
	unsigned long long BytesSent= 0;
	unsigned long long BytesRec= 0;
	BENCHMARK_RESULTS BenchmarkResults;
	
	HistBuffer = new unsigned char[CurrentHistTransferSize];
	outBuffer = new unsigned char[BENCHMARKHSBUFFERSIZE];
	inBuffer = new unsigned char[BENCHMARKHSBUFFERSIZE];
	
	if(outBuffer == NULL || inBuffer == NULL ||  HistBuffer == NULL)
	{
		printf("Buffers have not been allocated\n");
		return;
	}
	
	if(usbInfo[usbIndex].speed == 480)
		g_bHighSpeed = true;
	
	memset(&BenchmarkResults, 0, sizeof(BenchmarkResults));
	memset(outBuffer, 0, sizeof(outBuffer));
	memset(inBuffer, 0, sizeof(inBuffer));
	
	for (int i=0;i<MaxTransferSize;i++ ) //No incrementing patterns for benchmark test, constant byte only
		outBuffer[i] = ( unsigned char ) 55;
	
	//Get statistics - clears errors etc
	iRet = SendVendorCommand ( handle_udev, STATISTICS,0, bufferout, CHANGEMODEOUTSTAT);
	
	printf("Benchmark: READING FROM USB DEVICE\n");
	
	//Run benchmark test for 10 cycles (5 x send 5 x receive)
    while(benchmarkCycle < 10 && error_USB == false)
	{
		//Need to send the vendor command every time
		//Put plug into benchmark Mode	
		iRet = SendVendorCommand (handle_udev, BENCHMARK, 0, bufferout, CHANGEMODEOUTSTAT); //Set Firmware to Loopback Mode
		
		if ( iRet == 0xFF  || iRet <=0 )
		{
			//Plug might be disconnected
			printf("Unable to put plug into benchmark mode\n");
			error_USB = true;
			break;
		}
		
		//Must be a 10msec delay after sending a vendor command
		usleep(10000);
		
		//Read from plug
		if(ReadPhase)
		{
			dwBytesRead = 0;
			readRes = libusb_bulk_transfer ( handle_udev, EPBENCHIN, inBuffer, MaxTransferSize, &dwBytesRead, 1000 );
			
			if(readRes < 0)
			{
				printf("Benchmark read failed: %d\n", readRes);
				error_USB = true;
				break;
			}
			
			if(dwBytesRead > 0 )
				BytesRec += dwBytesRead;
		}
		else
		{
			//Write to plug EPBENCHOUT 
			dwBytesWritten = 0;
			writeRes = libusb_bulk_transfer ( handle_udev, EPBENCHOUT, outBuffer, MaxTransferSize, &dwBytesWritten, 1000 );
			
			if(writeRes != 0)
			{
				printf("Benchmark write failed: %d\n", writeRes);
				error_USB = true;
				break;
			}
			
			if(dwBytesWritten > 0 )
				BytesSent += dwBytesWritten;
			
		}
		
		//Get a history report
		memset(HistBuffer, 0, CurrentHistTransferSize);
		//Sleep 70ms
		usleep(70000);
		dwBytesRead = 0;
		readRes = libusb_bulk_transfer ( handle_udev, EPHISTORY, HistBuffer, CurrentHistTransferSize, &dwBytesRead, 1000 );
		
		// if the read of the History buffer failed, we want to stop the test
		if (readRes != 0) 
		{
			printf("Benchmark Statistics Read failed (%d)\n", readRes);
			continue;
		}
		
		//Calculate the Maximum & Average speed from the history report
		MaxHistory = 0;
		MinHistory = 99999;//Dummy high value
		iTotalHistory = 0;
		iTotalHistoryMeasured = 0;
		flAveHistoryPossible = 0;
		iFirstFrameVal = 0;
		iLastFrameVal = 0;
		NumFrames = 0;
		iLastFrameIndex = 0;
		TestInvalid = false;
		for (int i=0;i<CurrentHistTransferSize;i++) 
		{
			History[i] = (int) HistBuffer[i];
			if (History[i] != 0) 
			{
				if (History[i] == 255) 
				{
					printf("Benchmark: In Bulk NAK recieved - test invalid\n");
					TestInvalid = true;
				}
				else if (History[i] == 254) 
				{
					printf("Benchmark: Ping NAK received  - test invalid\n");
					TestInvalid = true;
				}
				else  
				{
					if (History[i] != 0)
					{
						iLastFrameIndex = i;
						NumFrames++;
					}
				}
			}
			
			//Check for new maximum value
			if (History[i] > MaxHistory)
				MaxHistory = History[i];
			
			iTotalHistoryMeasured += History[i];
			if (History[i] != 0)
				iLastFrameVal = History[i];
		}
		
		if (NumFrames > 2)	//Remove ist and last microframes
		{
			for (int i=1;i<iLastFrameIndex;i++) 
				if (History[i] > 0 && History[i] < MinHistory)	//Check for new minimum value, excluded 1st and last values
					MinHistory = History[i];
		}
		else				//Use all microframes
		{
			for (int i=0;i<=iLastFrameIndex;i++) 
				if (History[i] > 0 && History[i] < MinHistory)
					MinHistory = History[i];
		}
		
		iFirstFrameVal = History[0];
		if (NumFrames == 0)
			TestInvalid = true;
		
		if (TestInvalid) 
		{
			iNumTestInvalid++;
			
			if (iNumTestInvalid > 3)
			{
				iNumTestInvalid = 0;
			}
		} 
		else	
		{
			iNumTestInvalid = 0;
			
			if (NumFrames > 2)	//Remove 1st and last microframes
				flAveHistoryMeasured = (float)(iTotalHistoryMeasured - iFirstFrameVal - iLastFrameVal)/ (float)(NumFrames - 2);
			else				//Use all microframe data
				flAveHistoryMeasured = (float)iTotalHistoryMeasured/ (float)NumFrames;
			
			if (g_bHighSpeed) 
			{
				if (ReadPhase == true)
					g_flMaxReadRate = fmaxf(g_flMaxReadRate, ((float) MaxHistory*512*8)/125);
				else
					g_flMaxWriteRate = fmaxf(g_flMaxWriteRate, ((float) MaxHistory*512*8)/125);
				
				MaxRate = ((float) MaxHistory*512*8)/125;
				AverageRate = ((float) flAveHistoryMeasured*512*8)/125;
				MinRate = ((float) MinHistory*512*8)/125;
				
			}
			else 
			{ 
				//Fullspeed
				if (ReadPhase == true)
					g_flMaxReadRate = fmaxf(g_flMaxReadRate, ((float) MaxHistory*64*8)/1000);
				else
					g_flMaxWriteRate = fmaxf(g_flMaxWriteRate, ((float) MaxHistory*64*8)/1000);
				MaxRate = ((float) MaxHistory*64*8)/1000;
				AverageRate = ((float) flAveHistoryMeasured*64*8)/1000;
				MinRate = ((float) MinHistory*64*8)/1000;
				
			}
			
			
			if(ReadPhase == true)
			{
				if (MaxRate > BenchmarkResults.MaxReadSpeed)
					BenchmarkResults.MaxReadSpeed = MaxRate;
			}
			else
			{
				if (MaxRate > BenchmarkResults.MaxWriteSpeed)
					BenchmarkResults.MaxWriteSpeed = MaxRate;
			}
			
			if (MinRate < OverallMinRate && MinRate > 0)
				OverallMinRate = MinRate;
			
			//TotalOfAverageRate += (AverageRate + AverageRatePossible) / 2;
			TotalOfAverageRate += AverageRate ;
			
			// change to write operations to device after defined number of reads
			if (TestPass % READWRITECYCLES == 0)
			{
				//Start back at the orignial data block size
				if (g_bHighSpeed) 
				{
					MaxTransferSize = BENCHMARKHSBUFFERSIZE;
					FIFOSize = HSFIFOSIZE;						// Only used for reporting
				} 
				else 
				{
					MaxTransferSize = BENCHMARKFSBUFFERSIZE;
					FIFOSize = FSFIFOSIZE;						// Only used for reporting
				}
				
				if (ReadPhase == false)
				{
					benchmarkCycle++;
					ReadPhase = true;
					if(benchmarkCycle < BENCHMARKCYCLES)
						printf("Benchmark: READING FROM USB DEVICE\n");
					
				}
				else
				{
					benchmarkCycle++;
					ReadPhase = false;
					if(benchmarkCycle < BENCHMARKCYCLES)
						printf("Benchmark: WRITING TO USB DEVICE\n");
				}
			}
			
			
				// update pass counter
			TestPass++;
			OverallAverageRate = TotalOfAverageRate / TestPass;
			g_flMaxRate = OverallMaxRate;
			
		}	
		
	}
	
	printf("Max Read %0.0f Mbit/s\nMax Write %0.0f Mbit/s\nAverage Speed %0.0f Mbit/s\n", BenchmarkResults.MaxReadSpeed , BenchmarkResults.MaxWriteSpeed, OverallAverageRate);
	
	delete [] outBuffer;
	delete [] inBuffer;	
}




